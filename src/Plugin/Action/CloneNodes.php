<?php

namespace Drupal\clone_nodes\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an action that can clone nodes.
 *
 * @Action(
 *   id = "clone_nodes",
 *   label = @Translation("Clone Node"),
 *   type = "node"
 * )
 */
class CloneNodes extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a CloneNodes object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /* Clone the entity that was passed in. */
    if (!empty($entity) && is_object($entity)
    && method_exists($entity, 'createDuplicate')
    && is_callable([$entity, 'createDuplicate'])) {
      $new_node = $entity->createDuplicate();
      // New entity has been created, mirror the published/unpublished state.
      if ($entity->isPublished()) {
        $new_node->setPublished();
      }
      // If content_moderation is installed, mirror the moderation state.
      if ($this->moduleHandler->moduleExists('content_moderation')) {
        $moderation_state_full = $entity->get('moderation_state');
        if (!empty($moderation_state_full)) {
          $moderation_state = $moderation_state_full->getValue()[0]['value'];
          $new_node->set('moderation_state', $moderation_state);
        }
      }
      $new_node->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

}
