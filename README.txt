DESCRIPTION
-----------
Adds a 'Clone node' action to the admin/content page.
Works on all types of nodes, including those that contain paragraphs.
The new node is an exact clone of the original i.e. it will have exactly
the same title, all fields will be identical and even the publishing
state will be the same (if content_moderation is installed, the moderation
state will be the same).
The only way to distinguish the cloned node from the original is that the
nid will be different.

CONFIGURATION
-------------
No configuration required, just install the module.
